package agenda.model.repository.classes;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Activity;
import agenda.model.repository.interfaces.IRepositoryActivity;

public class RepositoryActivityMock implements IRepositoryActivity {

	private List<Activity> activities;
	
	public RepositoryActivityMock()
	{
		activities = new LinkedList<Activity>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			Activity act = new Activity("activity 1", df.parse("03/20/2018 12:00"),
					df.parse("03/20/2018 14:00"),
					null,
					"description 1");
			activities.add(act);
			act = new Activity("activity 2", df.parse("03/21/2018 12:00"),
					df.parse("03/21/2018 14:00"),
					null,
					"description 2");
			activities.add(act);
			act = new Activity("activity 3", df.parse("03/21/2018 13:00"),
					df.parse("03/21/2018 14:00"),
					null,
					"description 3");
			activities.add(act);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Activity> getActivities() {
		return activities;
	}

	@Override
	public boolean addActivity(Activity activity) {
		boolean result = true;
		for (Activity activity1 : activities) {
			if (activity.intersect(activity1))
				result = false;
		}
		if(result)
			if(!activities.contains(activity) && activity.isValid()){
				result = true;
				activities.add(activity);
			}
			else
				result = false;
		return result;
	}

	@Override
	public boolean removeActivity(Activity activity) {
		int index = activities.indexOf(activity);
		if (index<0) return false;
		activities.remove(index);
		return true;
	}

	@Override
	public boolean saveActivities() {
		return true;
	}

	@Override
	public int count() {
		return activities.size();
	}

	@Override
	public List<Activity> activitiesByName(String name) {
		List<Activity> result = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(name)) result.add(a);
		return result;
	}

	@Override
	public List<Activity> activitiesOnDate(Date d) {
		List<Activity> result = new ArrayList<Activity>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		for (Activity a : activities) {
            String dateFormat_d = dateFormat.format(d);
            String dateFormat_a = dateFormat.format(a.getStart());
            if (dateFormat_d.compareTo(dateFormat_a) == 0) {
                result.add(a);
            }
        }
		return result;
	}

}
