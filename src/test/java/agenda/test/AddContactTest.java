package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.IRepositoryContact;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class AddContactTest {

	private Contact con;
	private IRepositoryContact rep;
	
	@Before
	public void setUp() {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "strada Primaverii";
		String telefon = "0712345678";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1 + 1)
			assertTrue(true);
	}

	@Test
	public void testCase2()
	{
		int size1 = rep.getContacts().size();
		String nume = "";
		String adresa = "strada Primaverii";
		String telefon = "0712345678";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1)
			assertTrue(true);
	}

	@Test
	public void testCase3()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "";
		String telefon = "0712345678";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1)
			assertTrue(true);
	}

	@Test
	public void testCase4()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "strada Primaverii";
		String telefon = "07aa123123";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1)
			assertTrue(true);
	}

	@Test
	public void testCase5()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "strada Primaverii";
		String telefon = "074";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1)
			assertTrue(true);
	}

	@Test
	public void testCase6()
	{
		int size1 = rep.getContacts().size();
		String nume = "D";
		String adresa = "strada Primaverii";
		String telefon = "0712345678";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1 + 1)
			assertTrue(true);
	}

	@Test
	public void testCase7()
	{
		int size1 = rep.getContacts().size();
		char[] array = new char[255];
		int pos = 0;
		while (pos < 254) {
			array[pos] = 'a';
			pos++;
		}
		String nume = new String(array);
		assertTrue(nume.length() == 255);
		String adresa = "strada Primaverii";
		String telefon = "07aa123123";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1 + 1)
			assertTrue(true);
	}

	@Test
	public void testCase8()
	{
		int size1 = rep.getContacts().size();
		char[] array = new char[254];
		int pos = 0;
		while (pos < 253) {
			array[pos] = 'a';
			pos++;
		}
		String nume = new String(array);
		assertTrue(nume.length() == 254);
		String adresa = "strada Primaverii";
		String telefon = "07aa123123";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1 + 1)
			assertTrue(true);
	}

	@Test
	public void testCase9()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "strada Primaverii";
		String telefon = "071234567";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1)
			assertTrue(true);
	}

	@Test
	public void testCase10()
	{
		int size1 = rep.getContacts().size();
		String nume = "Dana";
		String adresa = "s";
		String telefon = "0712345678";
		try {
			con = new Contact(nume, adresa, telefon);
			rep.addContact(nume, adresa, telefon);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int size2 = rep.getContacts().size();
		if (size2 == size1 + 1)
			assertTrue(true);
	}


}
