package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.IRepositoryActivity;
import agenda.model.repository.interfaces.IRepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class IntegrationTestTopDown {

    private IRepositoryActivity repAct;
    private IRepositoryContact repCon;

    @Before
    public void setup() {
        repCon = new RepositoryContactMock();
        repAct = new RepositoryActivityMock();
    }

    @Test
    public void testUnitA(){
        int size1 = repCon.getContacts().size();
        try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";

            repCon.addContact(nume1, adresa1, telefon1);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        int size1_2 = repCon.getContacts().size();
        if (size1_2 == size1 + 1)
            assertTrue(true);

        int size2 = repCon.getContacts().size();
        try {
            String nume2 = "";
            String adresa2 = "strada Primaverii";
            String telefon2 = "0712345678";

            repCon.addContact(nume2, adresa2, telefon2);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        int size2_2 = repCon.getContacts().size();
        if (size2 == size1)
            assertTrue(true);
    }

    @Test
    public void testIntB() {
        // adaugarea unui contact
        int size1 = repCon.getContacts().size();
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";
            Contact contact = new Contact(nume1, adresa1, telefon1);
            repCon.addContact(nume1, adresa1, telefon1);
            contacts.add(contact);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        int size1_2 = repCon.getContacts().size();
        if (size1_2 == size1 + 1)
            assertTrue(true);

        // adaugarea unei activitati
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("activity1",
                    df.parse("03/20/2018 15:30"),
                    df.parse("03/20/2018 16:30"),
                    contacts,
                    "description1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }
    }

    @Test
    public void testIntC() {
        // adaugarea unui contact
        int size1 = repCon.getContacts().size();
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";
            Contact contact = new Contact(nume1, adresa1, telefon1);
            repCon.addContact(nume1, adresa1, telefon1);
            contacts.add(contact);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        int size1_2 = repCon.getContacts().size();
        if (size1_2 == size1 + 1)
            assertTrue(true);

        // adaugarea unei activitati
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("activity1",
                    df.parse("03/20/2018 15:30"),
                    df.parse("03/20/2018 16:30"),
                    contacts,
                    "description1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }

        // activitati dintr-o anumita data
        Calendar c = Calendar.getInstance();
        c.set(2018, 3 - 1, 20);
        List<Activity> result = repAct.activitiesOnDate(c.getTime());
        assertTrue(result.size() == 2);

    }

}
