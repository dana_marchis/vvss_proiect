package agenda.test;

import agenda.model.base.Activity;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.interfaces.IRepositoryActivity;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AddActivityTest {
	private Activity act;
	private IRepositoryActivity rep;
	
	@Before
	public void setUp() {
		rep = new RepositoryActivityMock();
	}
	
	@Test
	public void testCase1()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("activity1",
					df.parse("08/20/2018 13:30"),
					df.parse("08/20/2018 14:30"),
					null,
					"description1");
			boolean result = rep.addActivity(act);
			assertTrue(result);
		} catch (ParseException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testCase2()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			for (Activity a : rep.getActivities())
				rep.removeActivity(a);
			
			act = new Activity("activity2",
					df.parse("08/20/2018 15:00"),
					df.parse("08/20/2018 16:00"),
					null,
					"");
			boolean result = rep.addActivity(act);
			assertFalse(result);
		}
		catch(Exception e){
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase3()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("activity2",
					df.parse("08/20/2018 15:00"),
					df.parse("08/20/2018 16:00"),
					null,
					"description2");
			rep.addActivity(act);

			act = new Activity("activity3",
					df.parse("08/20/2018 15:30"),
					df.parse("08/20/2018 16:30"),
					null,
					"description3");
			boolean result = rep.addActivity(act);
			assertFalse(result);
		}
		catch(Exception e){
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase4() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("activity3",
					df.parse("08/20/2018 15:30"),
					df.parse("08/20/2018 16:30"),
					null,
					"description3");
			rep.addActivity(act);

			act = new Activity("activity3",
					df.parse("08/20/2018 15:30"),
					df.parse("08/20/2018 16:30"),
					null,
					"description3");

			boolean result = rep.addActivity(act);
			assertFalse(result);
		} catch (Exception e) {
			assertTrue(true);
		}
	}
}
