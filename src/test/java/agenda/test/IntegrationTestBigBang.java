package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.IRepositoryActivity;
import agenda.model.repository.interfaces.IRepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IntegrationTestBigBang {

	private IRepositoryActivity repAct;
	private IRepositoryContact repCon;

	@Before
	public void setup() {
		repCon = new RepositoryContactMock();
		repAct = new RepositoryActivityMock();
	}

	// testare modul A: adaugarea de contacte
	@Test
	public void testUnitA() {
		int size1 = repCon.getContacts().size();
		try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";

            repCon.addContact(nume1, adresa1, telefon1);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int size1_2 = repCon.getContacts().size();
		if (size1_2 == size1 + 1)
			assertTrue(true);

        int size2 = repCon.getContacts().size();
        try {
            String nume2 = "";
            String adresa2 = "strada Primaverii";
            String telefon2 = "0712345678";

            repCon.addContact(nume2, adresa2, telefon2);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        int size2_2 = repCon.getContacts().size();
        if (size2 == size1)
            assertTrue(true);
	}

    //	testare modul B: adaugare activitati
	@Test
	public void testUnitB() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("activity1",
                    df.parse("08/20/2018 13:30"),
                    df.parse("08/20/2018 14:30"),
                    null,
                    "description1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }

        try{
            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act2 = new Activity("activity2",
                    df.parse("08/20/2018 15:00"),
                    df.parse("08/20/2018 16:00"),
                    null,
                    "");
            boolean result = repAct.addActivity(act2);
            assertFalse(result);

            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act3 = new Activity("activity2",
                    df.parse("08/20/2018 15:00"),
                    df.parse("08/20/2018 16:00"),
                    null,
                    "description2");
            repAct.addActivity(act3);

            Activity act4 = new Activity("activity3",
                    df.parse("08/20/2018 15:30"),
                    df.parse("08/20/2018 16:30"),
                    null,
                    "description3");
            boolean result2 = repAct.addActivity(act4);
            assertFalse(result2);

            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act5 = new Activity("activity3",
                    df.parse("08/20/2018 15:30"),
                    df.parse("08/20/2018 16:30"),
                    null,
                    "description3");
            repAct.addActivity(act5);

            Activity act6 = new Activity("activity3",
                    df.parse("08/20/2018 15:30"),
                    df.parse("08/20/2018 16:30"),
                    null,
                    "description3");

            boolean result3 = repAct.addActivity(act6);
            assertFalse(result3);
        }
        catch(Exception e){
            assertTrue(true);
        }

    }

    // testare modul C: afisare activitati dintr-o anumita data
	@Test
	public void testUnitC() {
        Calendar c = Calendar.getInstance();
        c.set(2018, 3 - 1, 20);
        List<Activity> result = repAct.activitiesOnDate(c.getTime());
        assertTrue(result.size() == 1);

        c.set('a', 3 - 1, 20);
        try {
            repAct.activitiesOnDate(c.getTime());
        } catch (Exception e) {
            assertTrue(true);
        }
	}

	@Test
	public void testIntP(){
        // adaugarea unui contact
        int size1 = repCon.getContacts().size();
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";
            Contact contact = new Contact(nume1, adresa1, telefon1);
            repCon.addContact(nume1, adresa1, telefon1);
            contacts.add(contact);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        int size1_2 = repCon.getContacts().size();
        if (size1_2 == size1 + 1)
            assertTrue(true);

        // adaugarea unei activitati
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("activity1",
                    df.parse("03/20/2018 15:30"),
                    df.parse("03/20/2018 16:30"),
                    contacts,
                    "description1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }

        // activitati dintr-o anumita data
        Calendar c = Calendar.getInstance();
        c.set(2018, 3 - 1, 20);
        List<Activity> result = repAct.activitiesOnDate(c.getTime());
        assertTrue(result.size() == 2);
	}

}
